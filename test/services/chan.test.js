const assert = require('assert');
const app = require('../../src/app');

describe('\'chan\' service', () => {
  it('registered the service', () => {
    const service = app.service('chan');

    assert.ok(service, 'Registered the service');
  });
});
