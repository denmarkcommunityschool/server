# Use node 6.3.0 LTF
FROM node:10

WORKDIR /usr/src/sort-server

COPY package*.json ./

RUN npm install

COPY . .
EXPOSE 3030

CMD ["npm","start"] 