const users = require('./users/users.service.js');
const chat = require('./chat/chat.service.js');
const chan = require('./chan/chan.service.js');
const message = require('./message/message.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(chat);
  app.configure(chan);
  app.configure(message);
};
