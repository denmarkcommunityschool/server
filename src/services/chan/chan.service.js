// Initializes the `chan` service on path `/chan`
const createService = require('feathers-knex');
const createModel = require('../../models/chan.model');
const hooks = require('./chan.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'chan',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/chan', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('chan');

  service.hooks(hooks);
};
  