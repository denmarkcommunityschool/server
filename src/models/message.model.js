/* eslint-disable no-console */

// message-model.js - A KnexJS
// 
// See http://knexjs.org/
// for more of what you can do here.
module.exports = function (app) {
  const db = app.get('knexClient');
  const tableName = 'message';
  db.schema.hasTable(tableName).then(exists => {
    if(!exists) {
      db.schema.createTable(tableName, table => {
        table.uuid('id');
        table.string('content');
        table.string('Attachments');
        table.timestamp('createdAt', { precision: 6 }).defaultTo(Date.now());
        // Foreign Key
        table.uuid('chatId');
        table.foreign('chatId').references('chat.id').onDelete('CASCADE');
        
        table.uuid('channelId');
        table.foreign('channelId').references('chan.id').onDelete('CASCADE');

        table.uuid('userId');
        table.foreign('userId').references('users.id').onDelete('CASCADE');
      })
        .then(() => console.log(`Created ${tableName} table`))
        .catch(e => console.error(`Error creating ${tableName} table`, e));
    }
  });
  

  return db;
};
