/* eslint-disable no-console */

// chan-model.js - A KnexJS
// 
// See http://knexjs.org/
// for more of what you can do here.
module.exports = function (app) {
  const db = app.get('knexClient');
  const tableName = 'chan';
  db.schema.hasTable(tableName).then(exists => {
    if(!exists) {
      db.schema.createTable(tableName, table => {
        table.uuid('id');
        table.string('type');
        table.string('title');
        table.string('permissions');
        table.string('history');
        table.string('title');

        // Foreign Key

        table.uuid('chatId');
        table.foreign('chatId').references('chat.id').onDelete('CASCADE');
        
      })
        .then(() => console.log(`Created ${tableName} table`))
        .catch(e => console.error(`Error creating ${tableName} table`, e));
    }
  });
  

  return db;
};
