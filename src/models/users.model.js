/* eslint-disable no-console */

// users-model.js - A KnexJS
// 
// See http://knexjs.org/
// for more of what you can do here.
module.exports = function (app) {
  const db = app.get('knexClient');
  const tableName = 'users';
  db.schema.hasTable(tableName).then(exists => {
    if(!exists) {
      db.schema.createTable(tableName, table => {
        table.uuid('id');
      
        table.string('email').unique();
        table.string('username');
        table.string('password');
        table.string('image');
        // Generated short unique identifier to make usernames unique
        table.string('username_id');
        // User preferences for their chat client
        table.json('preferences');

        // OAuth id's
        table.string('auth0Id');
      
        table.string('googleId');
      
        table.string('githubId');
      
      })
        .then(() => console.log(`Created ${tableName} table`))
        .catch(e => console.error(`Error creating ${tableName} table`, e));
    }
  });

  return db;
};
