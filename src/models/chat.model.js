/* eslint-disable no-console */

// chat-model.js - A KnexJS
// 
// See http://knexjs.org/
// for more of what you can do here.
module.exports = function (app) {
  const db = app.get('knexClient');
  const tableName = 'chat';
  db.schema.hasTable(tableName).then(exists => {
    if(!exists) {
      db.schema.createTable(tableName, table => {
        table.uuid('id');
        table.string('cName');
        table.string('image');
        table.json('userRoles');
        // Foreign Key

        table.uuid('userId');
        table.foreign('userId').references('users.id').onDelete('CASCADE');
        
      })
        .then(() => console.log(`Created ${tableName} table`))
        .catch(e => console.error(`Error creating ${tableName} table`, e));
    }
  });
  

  return db;
};
